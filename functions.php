<?php
/**
 * Reikiwaters-theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package reikiwaters-theme
 */

add_action( 'wp_enqueue_scripts', 'storefront_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function storefront_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'storefront-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'reikiwaters-theme-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'storefront-style' )
	);

}

function storefront_credit() {
	// echo 'hallo';
}

remove_action(storefront_footer, storefront_credit,20);



function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
			global $template;
			print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );