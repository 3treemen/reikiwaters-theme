<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>
<style>
	.storefront-secondary-navigation.woocommerce-active .site-header .secondary-navigation {
		margin-bottom: 0;
		position: absolute;
		top: 0;
		right: 2rem;
		padding: 0.5rem;
	}
	.site-header {
		padding-top: 0;
	}
	.main-navigation ul.menu > li > a, 
	.main-navigation ul.nav-menu > li > a, 
	.site-header-cart .cart-contents {
    	padding: 0.5rem 1rem;
	}
	</style>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
			the_content();
			?>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
